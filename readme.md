# FlexiTask 
FlexiTask is a Task managing app that facilitates both fixed and flexi tasks. A fixed task
is simply a task that has a fixed due date, that could be a Birthday on the 21st of Feb or
a bill that needs paid every 2 weeks. A flexitask on the other hand is a recurring Task which 
needs to be completed "roughly" in the time period specified. Perhaps you want to clean the windows
every fortnight, but don't mind if it's a few days late or early. The flexitask app uses a urgency 
sorting algorithm to display the tasks which are MOST overdue (relative to their recurring frequency).
For instance, a yearly task that is 4 days overdue is deemed less overdue than a weekly task which is 2 days
overdue. 

## Installation

### Requirements
* Android Studio (LATEST VERSION)
* Internet connection for first compile
* Pixel phone + API >= 22 for emulator (haven't tested on lower yet or other phone models)

## How it works

The mainactivity (timeLineFragmentsContainer) is a viewpager containing the fragments for Fixed Timeline and Flexi Timeline,
this allows the user to swipe between each fragment. When these fragment classes are first created (onCreate() method is called) 
each class initializes its own loadermanager. Once this loadermanager is up and running, it calls the OnCreateLoader() method , 
which initializes a new CursorLoader. This cursor loader is given a projection (a string array of the columns the activity/fragment want),
the URI (like a URL but for database tables) and a sortOrder (Flexitask wants the data returned to be sorted by its priority calculation, 
whereas fixed task it wants the data ordered by due date) . This cursorloader in turn queries the contentresolver. 
The contentresolver “resolves” the URI to the custom content Provider (Task Provider). This provider acts as an interface to the database, 
it’s here that determines whether we are inserting, updating deleting or just querying data - and whether we want a specific row or the entire 
table. It performs the desired task and returns a Cursor with the data requested, back to the contentResolver, which returns back to the LoaderManager which passes it on to the cursorOnFinish() method. In this method we call the cursorAdapter swapCursor() method and pass it this newly returned cursor to process. 

By using the loader thread and abstracting this data retrieval process to loaders and content providers, we allow the user to continue 
using the UI with few interruptions. I also use a similar querying technique in the editor activities, in addition to a simple if() statement 
that allows the app to reuse the editor activities for both creation of a new task and updating an existing task.

The app had to account for when the user “updates” the task from either the flexi or fixed timeline fragment in the form of the “done” 
button. When selecting “done” (the tick), for a fixed task, the app will check if that task is recurring or not. If it is it will update 
the tasks due date to (the current due date + recurring period * 86400000 (milliseconds in a day)) to get the new due date. It then restarts 
the cursor loader to go and retrieve these new changes.

When a flexi task is first created the date-last-completed field for that row/task is set to the current date (in milliseconds) and the next due date(in milliseconds)
is derived from this by adding the recurring period * milliseconds in a day. When the user indicates they have finished an activity (by pressing the done button/ tick),
the date-last-completed field is set to the current time and the next due date is recalculated.The priority rating is calculated at two stages, one in when we query
the data in the Flexitask fragment loader and want the return data sorted by priority and the other in the cursoradapter where we assign a XML color element to
indicate the priority to the user.

## Algorithm
To determine a flexi-tasks priority the app takes todays date (in milliseconds) and subtracts 
the date the task was created/ last done (in milliseconds) - this represents the milliseconds since the task was last completed
, we then convert this to number of days by dividing by 86,400,000 (milliseconds in a day) and adding 1 (for the due day). The app then divides that number
by how ofen the task is set to recurre (ie: weekly task, daily task). The end result is a number that represents a percentage of how
complete/overdue/underdue a task is. For tasks that have a lower recurring frequency (ie daily), each day that task is overdue will 
increase the result relativily more than a task with a higher frequnecy (ie: yearly)



     Todays Date - Date Created)/86,400,000 + 1 

     -------------------------------------------
	            Recurring Frequency



## Code used
https://github.com/Clans/FloatingActionButton for the floating action button